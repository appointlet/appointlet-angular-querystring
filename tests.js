describe("querystring provider", function() {
  'use strict';

  beforeEach(function() {
    module('appointlet.querystring');
  });

  it("should be defined", inject(function(QueryString) {
    expect(QueryString).toBeDefined();
  }));
});
