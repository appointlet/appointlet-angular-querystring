(function() {
  'use strict';

  angular.module('appointlet.querystring', [])

  /**
   * @ngdoc service
   * @name querystring.service.QueryString
   * @module appointlet.querystring
   *
   * @description Helper functions for working with querystrings.
   */
  .service('QueryString', function() {
    /**
     * @ngdoc method
     * @name querystring.service.QueryString#encode
     * @description Converts an object to a querystring.
     * @param {object} params to encode into a querystring.
     */
    this.encode = function(params) {
      return _.map(params, function(val, key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(val);
      }).join('&');
    };

    /**
     * @ngdoc method
     * @name querystring.service.QueryString#decode
     * @description Converts a querystring into an object
     * @param  {string} querystring to decode into an object.
     */
    this.decode = function(querystring) {
      return _.object(_.map(querystring.split('&'), function(param) {
        var pieces = param.split('=');
        return [decodeURIComponent(pieces[0]), decodeURIComponent(pieces[1])];
      }));
    };
  });

})();
